﻿using MyCouch;
using NGeoHash;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace OfflineDb.MyCouch
{
    class Program
    {
        static void Main(string[] args)
        {
            var user = "admin";
            var pwd = "i6gtwRR27wjH";
            var host = "ec2-54-171-212-153.eu-west-1.compute.amazonaws.com:5984";
            var client = new MyCouchClient($"http://{user}:{pwd}@{host}", "restaurants_lite_bookable_only");
            var itemsToInsert = 10000;

            Console.WriteLine("Importing Restaurant data to CouchDb...");

            var repository = new RestaurantRepository(client);
            var restaurants = GenerateRestaurantData(itemsToInsert);

            try
            {
                var result = repository.BulkImport(restaurants);
                Console.WriteLine(result);
                Console.WriteLine("Bulk import finished.");

                //UpdateBulk(repository, restaurants);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            client.Dispose();
            Console.ReadKey();
        }

        private static void UpdateBulk(RestaurantRepository repository, IEnumerable<Restaurant> restaurants)
        {
            Thread.Sleep(10000);
            Console.WriteLine("Updating 5000 restaurants...");
            Console.WriteLine("");
            var restaurantsToUpdate = restaurants.ToList();
            var random = new Random();
            for (int i = 0; i < 5000; i++)
            {
                var randomIndex = random.Next(15000);
                var restaurant = restaurantsToUpdate.ElementAt(randomIndex);
                var remoteRest = repository.Get(restaurant.Id);
                if (remoteRest != null)
                {
                    remoteRest.Name = "Updated";
                    var response = repository.Update(remoteRest);
                    Thread.Sleep(60);
                    Console.Write(i + "." + response + " ");
                }
            }
        }

        public static IEnumerable<Restaurant> GenerateRestaurantData(int itemsToInsert)
        {
            string[] cuisines = { "Italian", "French", "Indian", "Spanish", "International", "Steaks", "Fish" };
            string[] idForBookings = { "259906", "104912", "105195", "106724", "106789", "107222", "107263" };
            var random = new Random();

            var restaurants = new List<Restaurant>();

            for (var i = 0; i < itemsToInsert; i++)
            {
                var latitude = GetRandomDouble(random, 50.933348, 53.377808);
                var longitude = GetRandomDouble(random, -2.426554, 0.442038);
                restaurants.Add(new Restaurant()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 8),
                    Latitude = latitude,
                    Longitude = longitude,
                    GeoHashFive = GeoHash.Encode(latitude, longitude, 5), // 5km
                    GeoHashSeven = GeoHash.Encode(latitude, longitude, 7), // 153m
                    IdForBooking = idForBookings[random.Next(idForBookings.Length)],
                    ImageId = Guid.NewGuid().ToString("N"),
                    Rating = random.Next(1, 5),
                    Country = "FR",
                    MinPrice = 35.50,
                    MaxPrice = 120.80,
                    Cuisine = cuisines[random.Next(cuisines.Length)],
                    Address = Guid.NewGuid().ToString().Replace('-', ' ')
                });
            }

            return restaurants;
        }

        public static double GetRandomDouble(Random random, double min, double max)
        {
            return min + (random.NextDouble() * (max - min));
        }
    }
}