﻿using MyCouch;
using MyCouch.Requests;
using MyCouch.Serialization;
using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OfflineDb.MyCouch
{
    public class RestaurantRepository
    {
        private readonly MyCouchClient _client;
        private readonly ISerializer _serializer;

        public RestaurantRepository(MyCouchClient client)
        {
            _client = client;
            _serializer = client.DocumentSerializer;
        }

        public string Add(Restaurant restaurant)
        {
            var restaurantJson = _serializer.Serialize(restaurant);

            using (_client)
            {
                var response = AsyncContext.Run(() => _client.Documents.PostAsync(restaurantJson));
                return response.StatusCode.ToString();
            }
        }

        public Restaurant Get(string id)
        {
            var response = AsyncContext.Run(() => _client.Entities.GetAsync<Restaurant>(id));
            
            return (response.IsSuccess) ? response.Content : null;
        }

        public string Update(Restaurant restaurant)
        {
            var restaurantJson = _serializer.Serialize(restaurant);

            var response = AsyncContext.Run(() => _client.Documents.PutAsync(restaurant.Id, restaurantJson));
            return response.StatusCode.ToString();
        }

        public string BulkImport(IEnumerable<Restaurant> restaurants)
        {
            var itemSizeInBytes = 500;
            var requestLimitInBytes = 1024 * 1024;
            var itemsPerRequest = requestLimitInBytes / itemSizeInBytes;
            var responseCodes = new List<string>();

            foreach (var restaurantBatch in restaurants.Batch(itemsPerRequest))
            {
                var bulkRequest = new BulkRequest();
                foreach (var restaurant in restaurantBatch)
                {
                    var restaurantJson = _serializer.Serialize(restaurant);
                    bulkRequest.Include(restaurantJson);
                }
                var response = AsyncContext.Run(() => _client.Documents.BulkAsync(bulkRequest));
                responseCodes.Add(response.StatusCode.ToString());
            }

            return $"{restaurants.Count()} items where inserted in {(restaurants.Count() / itemsPerRequest)+1} requests with the following response codes {FlattenStringArray(responseCodes)}";
        }

        public void WipeDb()
        {
            
        }

        private string FlattenStringArray(IEnumerable<string> collection)
        {
            string response = "";
            int index = 1;
            foreach (string statusCode in collection)
            {
                response += $"{index}.{statusCode} ";
                index++;
            }

            return response;
        }
    }
}
