﻿using MyCouch;
using Newtonsoft.Json;

namespace OfflineDb.MyCouch
{
    [Document(DocType = "rest")]
    public class Restaurant
    {
        public string Id { get; set; }
        public string Rev { get; set; }
        public string Name { get; set; } = string.Empty;
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string GeoHashFive { get; set; } = string.Empty;
        public string GeoHashSeven { get; set; } = string.Empty;
        public string IdForBooking { get; set; } = string.Empty;
        public string ImageId { get; set; }

        [JsonIgnore]
        public int Rating { get; set; }

        [JsonIgnore]
        public int ReviewCount { get; set; }

        public string Country { get; set; } = string.Empty;
        public double? MinPrice { get; set; }
        public double? MaxPrice { get; set; }
        public double? AvgMealSpend { get; set; }
        public double? AvgMain { get; set; }
        public string Cuisine { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
    }
}
